package com.qa.commoncode;

import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

public class BrowserCode {

	private static final Logger LOGGER = LogManager.getLogger(BrowserCode.class);

	public static WebDriver getDriver() {
		WebDriver driver = null;
		try {
			switch (System.getProperty("browser").toUpperCase()) {
			case "IE":

				InternetExplorerDriverManager.getInstance().setup();

				driver = new InternetExplorerDriver();

				driver.manage().window().maximize();
				LOGGER.info("Browser set as Internet Explorer.");
				break;

			case "FIREFOX":

				FirefoxDriverManager.getInstance().setup();

				driver = new FirefoxDriver();

				driver.manage().window().maximize();
				LOGGER.info("Browser set as Firefox.");
				break;

			case "CHROME":
			default:

				ChromeDriverManager.getInstance().setup();

				driver = new ChromeDriver();

				driver.manage().window().maximize();
				LOGGER.info("Browser set as Chrome.");
				break;
			}
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			Assert.fail("Failed while Initializing the browser. Error: " + e);
		}
		return driver;
	}
}
