package com.qa.commoncode;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CommonBase extends InitClass{

	private static final Logger LOGGER = LogManager.getLogger(CommonBase.class);

	public void click(By element, String name) {

		try {
			waitForVisibility(element);
			waitForClickable(element);
			driver.findElement(element).click();
			LOGGER.info("Performed click operation on the element " + name);
		} catch (Exception e) {
			LOGGER.error("Failed to perform click operation on the element " + name + "; ERROR: " + e);
			Assert.fail("Failed to perform click operation on the element " + name + "; ERROR: " + e);
		}
	}

	public void type(By element, String toType, String name) {

		try {
			waitForVisibility(element);
			WebElement we = driver.findElement(element);
			we.clear();
			we.sendKeys(toType);
			LOGGER.info("Performed type operation on the element " + name + "; Value entered: " + toType);
		} catch (Exception e) {
			LOGGER.error("Failed to perform type operation on the element " + name + "; ERROR: " + e);
			Assert.fail("Failed to perform type operation on the element " + name + "; ERROR: " + e);
		}
	}

	public void select(By element, String toSelect, String name) {

		try {
			waitForVisibility(element);
			Select select = new Select(driver.findElement(element));
			select.selectByVisibleText(toSelect);
			LOGGER.info("Performed select operation on the element " + name + "; Value selected: " + toSelect);
		} catch (Exception e) {
			LOGGER.error("Failed to perform select operation on the element " + name + "; ERROR: " + e);
			Assert.fail("Failed to perform select operation on the element " + name + "; ERROR: " + e);
		}
	}

	public void waitForVisibility(By element) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(element)));
	}

	public void waitForPresence(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForClickable(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(by));
	}
}
