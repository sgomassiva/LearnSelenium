package com.qa.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.qa.commoncode.BrowserCode;
import com.qa.commoncode.InitClass;
import com.qa.page.LoginPage;

public class LoginTest extends InitClass {

	private static final Logger LOGGER = LogManager.getLogger(LoginTest.class);
	LoginPage loginPage;

	@BeforeSuite
	public void LaunchBrowser() {
		driver = BrowserCode.getDriver();
	}

	@BeforeTest
	public void LaunchApplication() {
		driver.navigate().to("https://inia.objectfrontier.com/login");
		Assert.assertEquals(driver.getTitle(), "SANGAM");
	}

	@Test
	public void LoginApplication() {
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		loginPage.loginApplication("sivag", "Dracarys_004");
		Assert.assertEquals(driver.getTitle(), "iNia");
		LOGGER.info("Application Logged in successfully.");
	}

	@AfterSuite
	public void quit() {
		driver.quit();
	}
}
