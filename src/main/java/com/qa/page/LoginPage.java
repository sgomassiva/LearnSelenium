package com.qa.page;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.qa.commoncode.CommonBase;

public class LoginPage extends CommonBase {

	private static final Logger LOGGER = LogManager.getLogger(CommonBase.class);

	By userName = By.id("username");
	By password = By.id("password");
	By loginButton = By.xpath("//button[text()='Sign In']");

	public void loginApplication(String userName, String password) {

		try {
			type(this.userName, userName, "User Name");
			type(this.password, password, "Password");
			click(loginButton, "Login Button");
			Thread.sleep(10000);
			driver.navigate().refresh();
		} catch (Exception e) {
			LOGGER.error("Failed to Login in to application. Error: " + e);
			Assert.fail("Failed to Login in to application. Error: " + e);
		}
	}

}
